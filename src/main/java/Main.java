import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


/**
 * AUTHOR Maxim Filippov
 * HomeWorkTwo
 * Sber 2022
 *
 *
 * */

//В задании не указано нужно ли делать обязательным ввод с клавиатуры
// для каждого параметра - сделал так:


public class Main {

    public static void main(String[] args){



        UserReceiver userReceiver = new UserReceiver("Получатель","Получателев","Получателевич",
                1000, 11111, 1000, 1010101010);
        UserSender userSender = new UserSender("Отправитель","Отправителев","Отправителевич",
                1000,22222,1000,2020202202 );

        System.out.println("Здравствуйте!");
        System.out.println("Выберите тип операции:");
        System.out.println("1 - отправить с карты на карту");
        System.out.println("2 - отправить со счета на счет");
        System.out.println("3 - получить ФИО получателя и отправителя в двух регистрах:");
        Scanner input = new Scanner(System.in);
        String operations = input.nextLine();

        if (operations.equals("1")){
                userSender.infoCard();
                userReceiver.infoCard();
                new cardToCard(userSender, userReceiver, 123, 100, "Рубль");
                userSender.infoCard();
                userReceiver.infoCard();
                }
        if (operations.equals("2")) {
            userSender.infoBankAccount();
            userReceiver.infoBankAccount();
            new bankAccountToBankAccount(userSender, userReceiver, 123, 1000, "Рубль");
            userSender.infoBankAccount();
            userReceiver.infoBankAccount();
        }
        if (operations.equals("3")) {
            System.out.println("ФИО отправителя:" + userSender.firstName.toLowerCase() + " " + userSender.lastName.toLowerCase() + " " + userSender.middleName.toLowerCase());
            System.out.println("ФИО отправителя:" + userSender.firstName.toUpperCase() + " " + userSender.lastName.toUpperCase() + " " + userSender.middleName.toUpperCase());
            System.out.println("-------------------");
            System.out.println("ФИО получателя:" + userReceiver.firstName.toLowerCase() + " " + userReceiver.lastName.toLowerCase() + " " + userReceiver.middleName.toLowerCase());
            System.out.println("ФИО получателя:" + userReceiver.firstName.toUpperCase() + " " + userReceiver.lastName.toUpperCase() + " " + userReceiver.middleName.toUpperCase());
        }


    }

}


   class Transfer{
       int transferNumber = 123;
       int amountCard = 0;
       int amountBankAccount = 0;
       String currency = "";
       Receiver receiver;
       Sender sender;
       DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
       Date transferDate = new Date();
   }

   class cardToCard extends Transfer {
       cardToCard(UserSender userSender,UserReceiver userReceiver, int amountCard){
           this.sender=userSender;
           this.receiver=userReceiver;
           this.amountCard=amountCard;
       }


       cardToCard(UserSender userSender, UserReceiver userReceiver, int transferNumber,
                  int amountCard, String currency){

           this.transferNumber=transferNumber;
           this.amountCard=amountCard;
           this.currency=currency;
           this.receiver=userReceiver;
           this.sender=userSender;

           if (userSender != null){
               if(userSender.cardSenderAmount>= amountCard) {
                 userSender.cardSenderAmount -= amountCard;
                 userReceiver.cardReceiverAmount += amountCard;

                   System.out.println("Номер перевода:" + transferNumber + "," + "Дата:" + dateFormat.format(transferDate) + "," + "Номер карты получателя:"
                   + userReceiver.cardReceiverNumber + "," + "Номер карты отправителя:" + userSender.cardSenderNumber  + "," + "Валюта:" +
                                      currency + "," + "Cумма:" + amountCard + "," + "ФИО:" + userSender.firstName + " " + userSender.lastName + " " + userSender.middleName + " " +
                                      "ФИО:" + userReceiver.firstName + " " + userReceiver.lastName + " " + userReceiver.middleName);
               } else {
                   System.out.println("Отрицательный баланс:" + userSender.cardSenderAmount);
               }
           }
       }
   }

    class bankAccountToBankAccount extends Transfer {
        bankAccountToBankAccount(UserSender userSender, UserReceiver userReceiver, int transferNumber,
                                 int amountBankAccount, String currency){

            this.transferNumber=transferNumber;
            this.amountBankAccount=amountBankAccount;
            this.currency=currency;
            this.receiver=userReceiver;
            this.sender=userSender;

            if (userSender != null){
                if(userSender.bankAccountSenderAmount>= amountBankAccount) {
                    userSender.bankAccountSenderAmount -= amountBankAccount;
                    userReceiver.bankAccountReceiverAmount += amountBankAccount;

                    System.out.println("Номер перевода:" + transferNumber + "," + "Дата:" + dateFormat.format(transferDate) + "," + "Номер счета получателя:"
                                       + userReceiver.bankAccountReceiverNumber + "," +
                                       "Номер счета отправителя:" + userSender.bankAccountSenderNumber + "," +
                                       "Валюта:" +
                                       currency + "," + "Cумма:" + amountBankAccount + "," +
                                       "ФИО:" + userSender.firstName + " " + userSender.lastName  + " " + userSender.middleName  + " " +
                                       "ФИО:" + userReceiver.firstName + " " + userReceiver.lastName + " " + userReceiver.middleName);

                } else {
                    System.out.println("Отрицательный баланс:" + userSender.bankAccountSenderAmount);
                }
            }

    }
}


    class Sender{
        String firstName = "";
        String lastName = "";
        String middleName = "";
        int bankAccountSenderAmount;
        int bankAccountSenderNumber;
        int cardSenderAmount;
        int cardSenderNumber;

        @Override
        public boolean equals(Object obj){
            if (obj == this) {
                return true;
            }
            if (obj == null || obj.getClass() != this.getClass()) {
                return false;
            }

           Sender guest = (Sender) obj;
            return cardSenderAmount == guest.cardSenderAmount
                   && (lastName == guest.lastName
                       || (lastName != null && lastName.equals(guest.lastName))) && (middleName == guest.middleName
                        || (middleName != null && middleName.equals(guest.middleName) ));


        }


        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
            result = prime * result + cardSenderAmount;
            result = prime * result +
             ((middleName == null) ? 0 : middleName.hashCode());
            return result;

        }


        }






   class UserSender extends Sender{
        UserSender(String firstName,String lastName, String middleName){
            this.firstName=firstName;
            this.lastName=lastName;
            this.middleName=middleName;
        }

       UserSender(String firstName,String lastName, String middleName,
                int bankAccountSenderAmount, int bankAccountSenderNumber,
                  int cardSenderAmount, int cardSenderNumber){
           this.firstName=firstName;
           this.lastName=lastName;
           this.middleName=middleName;

           this.bankAccountSenderAmount=bankAccountSenderAmount;
           this.bankAccountSenderNumber=bankAccountSenderNumber;
           this.cardSenderAmount=cardSenderAmount;
           this.cardSenderNumber=cardSenderNumber;
       }


       public void infoBankAccount() {
           System.out.println("ФИО:"+ firstName + " " + lastName + " " + middleName);
           System.out.println("Баланс счета:" + bankAccountSenderAmount);
       }

       public void infoCard() {
           System.out.println("ФИО:"+ firstName + " " + lastName + " " + middleName);
           System.out.println("Баланс карты:" + cardSenderAmount);
       }




   }


   class Receiver{
       String firstName = "";
       String lastName = "";
       String middleName = "";
       int bankAccountReceiverAmount;
       int bankAccountReceiverNumber;
       int cardReceiverAmount;
       int cardReceiverNumber;
   }

    class UserReceiver extends Receiver{
        UserReceiver(String firstName,String lastName, String middleName){
            this.firstName=firstName;
            this.lastName=lastName;
            this.middleName=middleName;

        }

        UserReceiver(String firstName,String lastName, String middleName,
                     int bankAccountReceiverAmount, int bankAccountReceiverNumber,
                   int cardReceiverAmount, int cardReceiverNumber){
            this.firstName=firstName;
            this.lastName=lastName;
            this.middleName=middleName;

            this.bankAccountReceiverAmount=bankAccountReceiverAmount;
            this.bankAccountReceiverNumber=bankAccountReceiverNumber;
            this.cardReceiverAmount=cardReceiverAmount;
            this.cardReceiverNumber=cardReceiverNumber;
        }



        public void infoBankAccount() {
            System.out.println("ФИО:"+ firstName + " " + lastName + " " + middleName);
            System.out.println("Баланс счета:" + bankAccountReceiverAmount);
        }

        public void infoCard() {
            System.out.println("ФИО:"+ firstName + " " + lastName + " " + middleName);
            System.out.println("Баланс карты:" + cardReceiverAmount);
        }



    }



